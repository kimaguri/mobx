import './App.css';
import {observer} from "mobx-react";
import { Store } from "./store";

const store = new Store();

const App = () => {
  return (
    <div className="App">
     <div style={{color: store.color}}>{store.count}</div>
      <div className="buttonsWrapper">
        <button className="button" onClick={store.inc}>+</button>
        <button className="button" onClick={store.dec}>-</button>
      </div>
    </div>
  );
}

export default observer(App);
