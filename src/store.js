import {configure, makeAutoObservable, reaction, when} from "mobx";

configure({enforceActions: 'observed'});

export class Store {
  count = 0;

  get color() {
    return this.count > 5 ? 'green' : this.count < 0 ? 'red': 'black';
  }

  constructor() {
    makeAutoObservable(this);
    when(() => this.count > 10, () => alert('Pls stop!'))
    reaction(() => this.count < 0, () => alert('Better click plus more...'))
  }

  inc = () => this.count++;
  dec = () => this.count--;
}
